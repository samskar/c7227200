﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace GPL_APP
{
    /// <summary>
    /// Parseses the command passed as a text string in textbox or a file
    /// </summary>
    public class Parse : Compile
    {

        /// <summary>
        /// Canvas object for handling pen, colour and position
        /// </summary>
        Canvas canvas;
        /// <summary>
        /// For creating shapes 
        /// </summary>
        ShapeFactory factory;
        /// <summary>
        /// Stores individual shapes
        /// </summary>
        Shape shape;

        public bool isrunning = false;
        public bool checksyntax = false;
        public bool terminate = false;
        Syntax syntaxCheck;

        /// <summary>
        /// Stores Index of the line.
        /// </summary>
        int lineoffset = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="canvas"></param>
        public Parse(Canvas canvas)
        {
            this.canvas = canvas;
            

            syntaxCheck = new Syntax();
            factory = new ShapeFactory();
            
        }

        /// <summary>
        /// Draws from s single line command passed as a string.
        /// </summary>
        /// <param name="line">String of cmd to be parsed</param>
        /// <param name="resetall">Reset menuitem for reseting fields.</param>
        /// <param name="lineoffset">Offset of every line</param>
        /// <returns>Boolean</returns>
        public void DrawFromLine(String line, int lineoffset)
        {
            Console.WriteLine("****Inside DrawLine*****");
            String[] split = LineParser(line);
            //assign new variables or reassign previous variables

            if(split.Length == 2)
            {
                //convert all variables to value
                split[1] = syntaxCheck.VarToValue(split[1]);

                //command and parameters from te command line 
                String Command = split[0].Trim();
                shape = factory.getShape(Command);

                //stores parameters or other command like colors and on/off for pen
                string[] parameters = Array.ConvertAll(split[1].Split(','), s => s);

                //checks if the command parsed as returned a shape or not
                if (shape != null)
                {
                    try
                    {
                        int[] param = StringToInt(parameters);//tries to convert string to int
                        if (Command.Equals("circle") || Command.Equals("square"))
                        {
                            Console.WriteLine("HERE WITH " + canvas.colour + " " + param[0]);
                            shape.set(canvas.colour, canvas.xPos, canvas.yPos, param[0]);
                        }



                        if(param.Length == 2)
                        {
                            shape.set(canvas.colour, canvas.xPos, canvas.yPos, param[0], param[1]);
                        }
                        else if(param.Length > 2)
                        {
                            if (param.Length % 2 != 0) throw new InvalidParameterException();

                            Console.WriteLine(param.Length + "This is the length of the parameter");
                            int[] cordinates = new int[param.Length + 2];
                            cordinates[0] = canvas.xPos;
                            cordinates[1] = canvas.yPos;
                            int j = 0;
                            for(int i = 2; i<param.Length + 2; i++)
                            {
                                cordinates[i] = param[j];
                                j++;
                            }
                            shape.set(canvas.colour, cordinates);


                        }

                        Console.WriteLine("Shape Drawn " + Command);
                        shape.draw(canvas.g, canvas.fill);


                    }
                    catch (FormatException)
                    {

                        throw new InvalidParameterException();
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidParameterException();
                    }

                }
                else if (Command.Equals("drawTo") == true)
                {
                    Console.WriteLine("Hello");
                    Console.WriteLine(parameters[0]+ " " + parameters[1]);
                    try
                    {
                        
                        canvas.DrawLine(StringToInt(parameters));
                    }
                    catch (FormatException)
                    {
                        throw new InvalidParameterException();

                    }

                }
                else if (Command.Equals("moveTo") == true)
                {
                    int[] cursorpos = new int[100];
                    try
                    {
                        cursorpos = StringToInt(parameters);
                        
                    }
                    catch (Exception e)
                    {
                        throw new InvalidParameterException();
                    }
                    if(!checksyntax) canvas.ChangeCursorLocation(cursorpos); 
                }
                else if (Command.Equals("fill") == true)
                {
                    if (TrimLower(parameters[0]).Equals("on") == true) { if(!checksyntax) canvas.fill = true; }

                    else if (TrimLower(parameters[0]).Equals("off") == true) { if (!checksyntax) canvas.fill = false; }

                    else throw new InvalidParameterException();

                }
                else if (Command.Equals("pen") == true)
                {
                    if (TrimLower(parameters[0]).Equals("red")) { if (!checksyntax) canvas.SetBrushAndPenColor("red"); }
                    else if (TrimLower(parameters[0]).Equals("green")) { if(!checksyntax) canvas.SetBrushAndPenColor("green"); }
                    else if (TrimLower(parameters[0]).Equals("blue")) { if(!checksyntax) canvas.SetBrushAndPenColor("blue"); }
                    else if (TrimLower(parameters[0]).Equals("yellow")) { if(!checksyntax) canvas.SetBrushAndPenColor("yellow"); }
                    else if (TrimLower(parameters[0]).Equals("lime")) { if(!checksyntax) canvas.SetBrushAndPenColor("lime"); }
                    else if (TrimLower(parameters[0]).Equals("gold")) { if(!checksyntax) canvas.SetBrushAndPenColor("gold"); }
                    else if (TrimLower(parameters[0]).Equals("cyan")) { if(!checksyntax) canvas.SetBrushAndPenColor("cyan"); }
                    else if (TrimLower(parameters[0]).Equals("tomato")) { if(!checksyntax) canvas.SetBrushAndPenColor("tomato"); }
                    else if (TrimLower(parameters[0]).Equals("black")) { if(!checksyntax) canvas.SetBrushAndPenColor("black"); }
                    else if (TrimLower(parameters[0]).Equals("redgreen")) { if (!checksyntax) canvas.SetBrushAndPenColor("redgreen"); }
                    else throw new InvalidParameterException();
                }
              
                else
                {
                    Console.WriteLine("hello");
                    Console.WriteLine(Command);
                    //different error display for different textbox inputs.
                    if (lineoffset != 0)
                    {
                        errors.Add("Invalid command and parameter given at line: " + lineoffset + "\r\n");
                    }
                        
                    else
                        errors.Add("Invalid command and parameter given." + "\r\n");
                }

                //block which handles two words command ends
                Console.WriteLine(split);
            }
            else if (split.Length == 1)
            {

                if (line.Equals("clear") == true)
                {
                    canvas.Clear();

                }
                else if (line.Equals("run") == true)
                {
                    if(!isrunning) canvas.run = true;
                }
                else if (line.Equals("reset") == true)
                {
                    canvas.Reset();

                }
                else
                {
                    throw new InvalidCommandException("Invalid command");
                }
                //block which handles one line command ends
            }
            else if (split.Length > 2)
            {
                //more than two split is invalid syntax
                Console.WriteLine(split.Length + "not here" + split[0] + "," + split[1] + "," + split[2]);
                throw new InvalidCommandException("Invalid command");
            }

            //only for textcommands
            if(lineoffset != 0) canvas.InvokeDelegate();
            //DrawFromLine method ends

            Console.WriteLine("Exiting drawline with lineoffset " + lineoffset);
        }

  

        /// <summary>
        /// Parses the Multiline command passed as a text.
        /// </summary>
        /// <param name="text">Multiline text command stored as a string</param>
        /// <param name="resetall">Multiline text command stored as a string</param>
        public void DrawFromText(String text)
        {
            String line = (text);
            string[] cmd = TextParser(line);
            Thread t1 = new Thread(()=> {
                SyntaxParser(cmd);
            });
            t1.Start();
            syntaxCheck.methods.Clear();//clear all the methods that is saved 
            syntaxCheck.variables.Clear();//clear all the variables that is saved
            //}

        }

        /// <summary>
        /// Converts array of string to an array of int.
        /// </summary>
        /// <param name="str">Array of string</param>
        /// <returns>Array of integers</returns>
        public int[] StringToInt(String[] str)
        {
            return Array.ConvertAll(str, s => int.Parse(s));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="resetall"></param>
        public void SyntaxParser(String[] cmd)
        {
            Console.WriteLine("*****Inside SyntaxParser*****");

            Console.WriteLine("******Thread T1 started*****");
            //disable btn after start of this thread
            canvas.InvokeBtnDelegate(false);
            StringBuilder consoleText = new StringBuilder();
            isrunning = true;
            bool[] isloop = new bool[100];
            int[] loopindex = new int[100];
            bool methodLookahead = false;
            Methods method = new Methods();
            int whilecount = -1;

            Hashtable ifendifoffset = syntaxCheck.WhileIfSyntaxCheck(cmd, new string[] { "if", "endif" });
            Hashtable whileendloopoffset = syntaxCheck.WhileIfSyntaxCheck(cmd, new string[] { "while", "endloop" });
            Hashtable methodloopoffset = syntaxCheck.WhileIfSyntaxCheck(cmd, new string[] { "method", "endmethod" });


            try
            {

                for (int i = 0; i < cmd.Length; i++)
                {
                    if (terminate) {  terminate = false; break;  } //break the loop 
                    this.lineoffset = i;
                    lineoffset++;
                    //first check if the line contains variable declaration or reassignment
                    if (cmd[i].Contains("=") || syntaxCheck.CheckForMethod(cmd[i]))
                    {
                        //don't forget to check method param length eual args length or not
                        if (syntaxCheck.CheckForMethod(cmd[i]) && !methodLookahead)
                        {
                            string[] newcmd = syntaxCheck.ParseMethodParameter(cmd[i]);
                            //Console.WriteLine(((Methods)syntaxCheck.methods[0]).methodName);
                            if (newcmd.Length > 0)
                            {
                                SyntaxParser(newcmd);
                                Console.WriteLine("var count before " + syntaxCheck.variables.Count);
                                syntaxCheck.RemoveVar(cmd[i]);

                                Console.WriteLine("var count after " + syntaxCheck.variables.Count);
                            }
                            else Console.WriteLine("Invalid Method Name or Parameter given");

                        }

                        if (cmd[i].Contains("=")) syntaxCheck.VarSyntaxCheck(cmd[i]);


                    }
                    //if the line contains if and endif statement
                    else if (syntaxCheck.FindWord(cmd[i], "if") || syntaxCheck.FindWord(cmd[i], "endif"))
                    {

                        if (ifendifoffset == null)
                        {
                            errors.Add("Invalid if syntax without proper ending!");
                            break;
                        }
                        //if line contains if
                        else if (syntaxCheck.FindWord(cmd[i], "if"))
                        {

                            //if condition is false
                            if (!syntaxCheck.CheckCondition(cmd[i]))
                            {
                                //if the statement has proper ending
                                for (int j = i; j < cmd.Length; j++)
                                {
                                    //if has proper ending go to the index of endif
                                    if (syntaxCheck.FindWord(cmd[j], "endif") && ifendifoffset.Contains(i) && (((int)ifendifoffset[i]) == j))
                                    {

                                        i = j;
                                    }
                                }
                            }
                            //if condition is true keep the index value of endif for other if's
                            else if (syntaxCheck.CheckCondition(cmd[i]))
                            {
                                //do nothing for now.
                            }
                        }

                    }
                    //if syntax checking ends
                    else if (syntaxCheck.FindWord(cmd[i], "while") || syntaxCheck.FindWord(cmd[i], "endloop"))
                    {
                        if (whileendloopoffset == null)
                        {
                            errors.Add("Invalid while syntax without proper ending!");
                            break;
                        }
                        //check for while keyword
                        else if (syntaxCheck.FindWord(cmd[i], "while"))
                        {

                            //for condition true
                            if (syntaxCheck.CheckCondition(cmd[i]))
                            {
                                //increase the whilecount to make it a unique index
                                //set isloop true and store loopindex to the unique index
                                whilecount++;
                                isloop[whilecount] = true;
                                loopindex[whilecount] = i;
                            }
                            //for false while condition
                            else if (!syntaxCheck.CheckCondition(cmd[i]))
                            {
                                //find the endloop keyword and if it's assosiated with this while move to that index.
                                for (int j = i; j < cmd.Length; j++)
                                {
                                    if (syntaxCheck.FindWord(cmd[j], "endloop") && whileendloopoffset.Contains(i) && (((int)whileendloopoffset[i]) == j))
                                    {
                                        i = j;
                                    }

                                }
                            }

                        }
                        //now if endloop is found and condition is false for this specific while block
                        //change isloop to false for this while block and decrease the whilecount to match with parent loop
                        //else condition is true for this block move to that index.
                        else if (syntaxCheck.FindWord(cmd[i], "endloop"))
                        {
                            if (!syntaxCheck.CheckCondition(cmd[loopindex[whilecount]]))
                            {
                                isloop[whilecount] = false;
                                whilecount--;
                            }
                            else i = loopindex[whilecount];


                        }

                    }
                    else if (syntaxCheck.FindWord(cmd[i], "method") || methodLookahead || syntaxCheck.FindWord(cmd[i], "endmethod"))
                    {
                        //if endmethod is found in the cmd then make sure to throw error at the begining.
                        if (syntaxCheck.FindWord(cmd[i], "method") && !methodLookahead)
                        {
                            if (methodloopoffset == null)
                            {
                                errors.Add("Invalid method declaration doesn't have ending : " + lineoffset);
                            }
                            //ParseMethod stores new method
                            if (syntaxCheck.ParseMethod(cmd[i], method))
                            {
                                methodLookahead = true;
                            }
                            else
                            {
                                errors.Add("Invalid method declaration at line : " + lineoffset);
                                break;
                            }
                        }
                        else if (methodLookahead && syntaxCheck.FindWord(cmd[i], "endmethod"))
                        {
                            methodLookahead = false;
                            Methods tempmethod = new Methods();
                            tempmethod.methodName = method.methodName;
                            tempmethod.parameters = method.parameters;
                            foreach (string s in method.content)
                            {
                                tempmethod.AddContent(s);
                            }
                            syntaxCheck.methods.Add(tempmethod);

                            //also call a method here that stores all the line,methodname and params
                        }
                        else if (methodLookahead)
                        {
                            //store current line in something.
                            method.AddContent(cmd[i]);
                        }

                    }
                    else
                    {
                        try
                        {
                            DrawFromLine(cmd[i], lineoffset);

                        }
                        catch (InvalidCommandException a)
                        {

                            errors.Add("Invalid command at line : " + lineoffset + "\r\n");
                            Console.WriteLine(a);
                            canvas.InvokeDelegate();
                        }
                        catch (InvalidParameterException)
                        {
                            errors.Add("Parameter given for the command is invalid at: " + lineoffset + "\r\n");
                            canvas.InvokeDelegate();
                        }
                        catch (IndexOutOfRangeException e)
                        {
                            Console.WriteLine(e);
                            canvas.InvokeDelegate();
                        }



                    }
                        //main loop ends here

                            
                        Thread.Sleep(1);
                }
                    
                    
            }
            catch (InvalidCastException)
            {
                errors.Add("Invalid condition given for if or while syntax!");
            }
            catch (InvalidCommandException)
            {
                errors.Add("Invalid syntax at line: " + lineoffset);
            }
            catch (FormatException)
            {
                errors.Add("Invalid value Assigned to the variable at line: " + lineoffset + "\r\n");
            }
            catch (EvaluateException)
            {
                errors.Add("Invalid value reassigned at line: " + lineoffset + "\r\n");
            }
            catch (ArgumentException)
            {
                errors.Add("Arguments given don't match parameters for the method at line : " + lineoffset);
            }


            /*
             * Process to be move to thread end because it should be executed at the end only once but this method runs multiple times.
             */

            //Get all the errors from parser and display it on console
            if (errors.Count != 0)
            {
                foreach (var item in errors)
                    consoleText.Append(item.ToString());
            }
            else if (errors.Count == 0 && checksyntax) consoleText.Append("No errors found!");
            canvas.InvokeConsoleDelegate(consoleText);

            //after computing everyting draw and set checksyntax to false
            isrunning = false;
            ClearErrors();
            checksyntax = false;
            terminate = false;
            canvas.InvokeBtnDelegate(true);//enable run btn at the end of the thread.
            Console.WriteLine("Check syntax value changed to : " + checksyntax);
            
        }//syntax parser ends



            


        

    }
}
