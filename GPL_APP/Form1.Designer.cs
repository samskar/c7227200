﻿namespace GPL_APP
{
    partial class GplApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GplApplication));
            this.cmdLine = new System.Windows.Forms.TextBox();
            this.console = new System.Windows.Forms.TextBox();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFile = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syntaxCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdText = new System.Windows.Forms.RichTextBox();
            this.fillcolor_label = new System.Windows.Forms.Label();
            this.copyright = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.error_label = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_Program = new System.Windows.Forms.Label();
            this.lbl_Display_Output = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_TitleCmd_List = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_run = new System.Windows.Forms.Button();
            this.displayWindow = new System.Windows.Forms.PictureBox();
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdLine
            // 
            this.cmdLine.AccessibleName = "commandTextBox";
            this.cmdLine.BackColor = System.Drawing.SystemColors.Window;
            this.cmdLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdLine.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cmdLine.Location = new System.Drawing.Point(12, 353);
            this.cmdLine.Name = "cmdLine";
            this.cmdLine.Size = new System.Drawing.Size(383, 30);
            this.cmdLine.TabIndex = 1;
            this.cmdLine.TextChanged += new System.EventHandler(this.cmdLine_TextChanged);
            this.cmdLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdLine_KeyDown);
            // 
            // console
            // 
            this.console.BackColor = System.Drawing.SystemColors.Window;
            this.console.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.console.ForeColor = System.Drawing.SystemColors.WindowText;
            this.console.Location = new System.Drawing.Point(12, 450);
            this.console.Multiline = true;
            this.console.Name = "console";
            this.console.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.console.Size = new System.Drawing.Size(443, 108);
            this.console.TabIndex = 6;
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1183, 30);
            this.menuBar.TabIndex = 5;
            this.menuBar.Text = "menuBar";
            this.menuBar.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuBar_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFile,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItem.Image")));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShowShortcutKeys = false;
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(71, 26);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // loadFile
            // 
            this.loadFile.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.loadFile.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.loadFile.Image = ((System.Drawing.Image)(resources.GetObject("loadFile.Image")));
            this.loadFile.Name = "loadFile";
            this.loadFile.Size = new System.Drawing.Size(141, 26);
            this.loadFile.Text = "Load";
            this.loadFile.Click += new System.EventHandler(this.loadFile_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.saveToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.exitToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syntaxCheckToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.helpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripMenuItem.Image")));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(85, 26);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // syntaxCheckToolStripMenuItem
            // 
            this.syntaxCheckToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.syntaxCheckToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.syntaxCheckToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("syntaxCheckToolStripMenuItem.Image")));
            this.syntaxCheckToolStripMenuItem.Name = "syntaxCheckToolStripMenuItem";
            this.syntaxCheckToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.syntaxCheckToolStripMenuItem.Text = "Syntax Check";
            this.syntaxCheckToolStripMenuItem.Click += new System.EventHandler(this.SyntaxCheckEvent);
            // 
            // cmdText
            // 
            this.cmdText.BackColor = System.Drawing.SystemColors.Window;
            this.cmdText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cmdText.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cmdText.Location = new System.Drawing.Point(12, 76);
            this.cmdText.Name = "cmdText";
            this.cmdText.Size = new System.Drawing.Size(444, 271);
            this.cmdText.TabIndex = 9;
            this.cmdText.Text = "";
            this.cmdText.TextChanged += new System.EventHandler(this.cmdText_TextChanged);
            // 
            // fillcolor_label
            // 
            this.fillcolor_label.BackColor = System.Drawing.SystemColors.ControlDark;
            this.fillcolor_label.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillcolor_label.ForeColor = System.Drawing.SystemColors.WindowText;
            this.fillcolor_label.Location = new System.Drawing.Point(368, 724);
            this.fillcolor_label.Name = "fillcolor_label";
            this.fillcolor_label.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.fillcolor_label.Size = new System.Drawing.Size(831, 16);
            this.fillcolor_label.TabIndex = 10;
            this.fillcolor_label.Text = "Fill : ❌ / Color : Black";
            this.fillcolor_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // copyright
            // 
            this.copyright.BackColor = System.Drawing.SystemColors.ControlDark;
            this.copyright.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyright.ForeColor = System.Drawing.SystemColors.ControlText;
            this.copyright.Location = new System.Drawing.Point(-20, 724);
            this.copyright.Name = "copyright";
            this.copyright.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.copyright.Size = new System.Drawing.Size(457, 16);
            this.copyright.TabIndex = 10;
            this.copyright.Text = "Copyright @ Samskar Koirala";
            this.copyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(344, 51);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Size = new System.Drawing.Size(93, 22);
            this.label3.TabIndex = 12;
            this.label3.Text = "Canvas";
            // 
            // error_label
            // 
            this.error_label.BackColor = System.Drawing.SystemColors.Window;
            this.error_label.ForeColor = System.Drawing.Color.Red;
            this.error_label.Location = new System.Drawing.Point(72, 389);
            this.error_label.Name = "error_label";
            this.error_label.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.error_label.Size = new System.Drawing.Size(383, 23);
            this.error_label.TabIndex = 8;
            this.error_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label4.Location = new System.Drawing.Point(14, 389);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 3);
            this.label4.Size = new System.Drawing.Size(51, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "Error";
            // 
            // lbl_Program
            // 
            this.lbl_Program.BackColor = System.Drawing.Color.LightGray;
            this.lbl_Program.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Program.ForeColor = System.Drawing.Color.Black;
            this.lbl_Program.Location = new System.Drawing.Point(13, 45);
            this.lbl_Program.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Program.Name = "lbl_Program";
            this.lbl_Program.Size = new System.Drawing.Size(442, 28);
            this.lbl_Program.TabIndex = 14;
            this.lbl_Program.Text = "Command";
            this.lbl_Program.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_Display_Output
            // 
            this.lbl_Display_Output.BackColor = System.Drawing.Color.LightGray;
            this.lbl_Display_Output.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Display_Output.ForeColor = System.Drawing.Color.Black;
            this.lbl_Display_Output.Location = new System.Drawing.Point(463, 45);
            this.lbl_Display_Output.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Display_Output.Name = "lbl_Display_Output";
            this.lbl_Display_Output.Size = new System.Drawing.Size(714, 28);
            this.lbl_Display_Output.TabIndex = 15;
            this.lbl_Display_Output.Text = "Command Output";
            this.lbl_Display_Output.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.LightGray;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 419);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(442, 28);
            this.label2.TabIndex = 16;
            this.label2.Text = "Console";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_TitleCmd_List
            // 
            this.lbl_TitleCmd_List.BackColor = System.Drawing.Color.LightGray;
            this.lbl_TitleCmd_List.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TitleCmd_List.ForeColor = System.Drawing.Color.Black;
            this.lbl_TitleCmd_List.Location = new System.Drawing.Point(13, 561);
            this.lbl_TitleCmd_List.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_TitleCmd_List.Name = "lbl_TitleCmd_List";
            this.lbl_TitleCmd_List.Size = new System.Drawing.Size(1164, 28);
            this.lbl_TitleCmd_List.TabIndex = 17;
            this.lbl_TitleCmd_List.Text = "Sample Commands";
            this.lbl_TitleCmd_List.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(14, 592);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1163, 120);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btn_run
            // 
            this.btn_run.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_run.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.btn_run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_run.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.btn_run.Image = ((System.Drawing.Image)(resources.GetObject("btn_run.Image")));
            this.btn_run.Location = new System.Drawing.Point(401, 353);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(54, 26);
            this.btn_run.TabIndex = 3;
            this.btn_run.UseVisualStyleBackColor = false;
            this.btn_run.Click += new System.EventHandler(this.textCompile);
            // 
            // displayWindow
            // 
            this.displayWindow.BackColor = System.Drawing.Color.White;
            this.displayWindow.Location = new System.Drawing.Point(462, 76);
            this.displayWindow.Name = "displayWindow";
            this.displayWindow.Size = new System.Drawing.Size(715, 482);
            this.displayWindow.TabIndex = 2;
            this.displayWindow.TabStop = false;
            this.displayWindow.Click += new System.EventHandler(this.displayWindow_Click);
            this.displayWindow.Paint += new System.Windows.Forms.PaintEventHandler(this.displayWindow_Paint);
            // 
            // GplApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1183, 741);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbl_TitleCmd_List);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_Display_Output);
            this.Controls.Add(this.lbl_Program);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.fillcolor_label);
            this.Controls.Add(this.cmdText);
            this.Controls.Add(this.error_label);
            this.Controls.Add(this.console);
            this.Controls.Add(this.btn_run);
            this.Controls.Add(this.displayWindow);
            this.Controls.Add(this.cmdLine);
            this.Controls.Add(this.menuBar);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuBar;
            this.Name = "GplApplication";
            this.Text = " ";
            this.Load += new System.EventHandler(this.GplApplication_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayWindow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox cmdLine;
        private System.Windows.Forms.PictureBox displayWindow;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.TextBox console;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFile;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.RichTextBox cmdText;
        private System.Windows.Forms.Label fillcolor_label;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.ToolStripMenuItem syntaxCheckToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label error_label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Program;
        private System.Windows.Forms.Label lbl_Display_Output;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_TitleCmd_List;
        private System.Windows.Forms.TextBox textBox1;
    }
}

