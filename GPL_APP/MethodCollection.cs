﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_APP
{
    public class MethodCollection : IAbstractCollection
    {
        private ArrayList _method = new ArrayList();

        public Iterator CreateIterator()
        {
            return new Iterator(this);
        }

        // Gets item count

        public int Count
        {
            get { return _method.Count; }
        }

        //Clear the item
        public void Clear()
        {
            _method.Clear();
        }

        // Indexer

        public object this[int index]
        {
            get { return _method[index]; }
            set { _method.Add(value); }
        }

        //Adding items
        public void Add(Methods value)
        {
            _method.Add(value);
        }
    }
}
