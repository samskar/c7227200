﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPL_APP;
using System.Collections;

namespace GPL_APP_TESTS
{
    [TestClass]
    public class SyntaxCheck
    {
        [TestMethod]
        public void VarSyntaxCheckTest()
        {

            Syntax syntax = new Syntax();

            bool result = syntax.VarSyntaxCheck("x = 10");
            
            Assert.AreEqual(true, result,  "Variable was initialized");

            ICollection key = syntax.variables.Keys;

            foreach (string k in key)
            {
                Console.WriteLine(k + "  " + syntax.variables[k].ToString());
                StringAssert.Contains("x", k);
                StringAssert.Contains("10", syntax.variables[k].ToString());

            }
        }

        [TestMethod]
        public void CheckConditionTest()
        {
            Syntax syntax = new Syntax();
            bool result = syntax.CheckCondition("if 5>2");
            Assert.AreEqual(result, true, "Condition was false instead of true");
            result = syntax.CheckCondition("if 5<2");
            Assert.AreEqual(result, false, "Condition was false instead of true");
        }

        [TestMethod]
        public void IfSyntaxCheckTest()
        {
            Syntax syntax = new Syntax();
            string[] ifcmd = { "if condition", "expression" , "endif" , "if condition", "expression", "endif" }; 


            Hashtable result = syntax.WhileIfSyntaxCheck(ifcmd, new string[] {"if", "endif" });

            Assert.AreEqual(result.Count, 2, "All the if else wasn't found!");
        }

        [TestMethod]
        public void WhileSyntaxCheckTest()
        {
            Syntax syntax = new Syntax();
            string[] ifcmd = { "while condition", "expression", "while condition", "expression", "endloop","endloop", "while condition", "expression", "endloop" };


            Hashtable result = syntax.WhileIfSyntaxCheck(ifcmd, new string[] { "while", "endloop" });

            Assert.AreEqual(result.Count, 3, "All the if else wasn't found!");
        }

        [TestMethod]
        public void ParseMethodTest()
        {
            Syntax syntax = new Syntax();
            string line = "method test(param1,param2)";
            Methods method = new Methods();

            bool result = syntax.ParseMethod(line, method);

            Assert.AreEqual(result, true, "Result came false instead of true!");
            StringAssert.Contains("test", method.methodName);
            StringAssert.Contains("param1", method.parameters[0]);
            StringAssert.Contains("param2", method.parameters[1]);
        }
    }
}
