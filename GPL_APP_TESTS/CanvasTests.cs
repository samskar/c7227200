﻿using System;
using System.Drawing;
using GPL_APP;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPL_APP_TESTS
{
    [TestClass]
    public class CanvasTests
    {
        [TestMethod]
        public void TestingMoveToCommand()
        {
            System.Windows.Forms.PictureBox display = new System.Windows.Forms.PictureBox();
            Canvas canvass;
            Parse parse;
            canvass = new Canvas(Graphics.FromImage(new Bitmap(500, 400)), display);
            parse = new Parse(canvass);

            parse.DrawFromLine("moveTo 150,150", 0);
            int xPos = canvass.xPos;

            Assert.AreEqual(150, xPos, 0.001, "xPOS value didn't change after moving the cursor");
        }
        
        [TestMethod]
        public void TestingDrawLineCommand()
        {
            System.Windows.Forms.PictureBox display = new System.Windows.Forms.PictureBox();
            Canvas canvass;
            Parse parse;
            canvass = new Canvas(Graphics.FromImage(new Bitmap(500, 400)), display);
            parse = new Parse(canvass);

            parse.DrawFromLine("drawTo 150,150", 0);
            int xPos = canvass.xPos;

            Assert.AreEqual(150, xPos, 0.001, "xPOS value didn't change after drawing a line");
        }
    }
}
